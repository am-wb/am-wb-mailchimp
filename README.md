# Angular Material Weburger MailChimp

See more details in [technical document](https://am-wb.gitlab.io/am-wb-mailchimp/doc/index.html).

This package is depricated.

## Development

We use these tools:

- nodejs
- grunt-cli
- bower

## Build

To build product from source you can run following commands respectively:

	nmp install
	bower install
	grunt build
	
## Rund demo

To run a demo type following commands respectively:

	npm install
	bower install
	grunt demo
	
## Use as library

This project is accessable by bower. To use this by bower run following command:

	bower install am-wb-mailchimp