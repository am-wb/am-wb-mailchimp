/* 
 * The MIT License (MIT)
 * 
 * Copyright (c) 2016 weburger
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';

angular.module('am-wb-mailchimp', [ 'am-wb-core'])
.config(function ($sceDelegateProvider) {
    $sceDelegateProvider.resourceUrlWhitelist([
		'self', 
		'**.list-manage.com/subscribe/post-json'
	]);
});


/* 
 * The MIT License (MIT)
 * 
 * Copyright (c) 2016 weburger
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';
/**
 * 
 */
angular.module('am-wb-mailchimp')
/**
 * 
 */

.config(['wbIconServiceProvider', function(wbIconServiceProvider) {
	wbIconServiceProvider
	// Move actions
	.addShape('wb-mailchimp', '<path d="m 5.6564568,15.115875 c -0.053986,-0.04125 -0.079105,-0.07575 -0.093955,-0.129525 -0.022076,-0.0978 -0.010574,-0.15465 0.076911,-0.2133 0.067367,-0.04425 0.1216206,-0.06525 0.1218014,-0.09315 0.00305,-0.0525 -0.2223606,-0.10635 -0.3799485,0.042 -0.1308739,0.1332 -0.1721793,0.41085 0.036581,0.6273 0.2323999,0.2397 0.5938341,0.296175 0.6501554,0.59745 0.0074,0.04275 0.012909,0.0903 0.00904,0.137175 8.25e-4,0.054 -0.018035,0.1314 -0.018971,0.136275 -0.06989,0.291075 -0.3589656,0.568725 -0.8355973,0.497925 -0.087807,-0.012 -0.1447105,-0.0225 -0.1616367,6.75e-4 -0.036754,0.04875 0.1675015,0.27225 0.5414437,0.26415 0.5339908,-0.0105 0.9756301,-0.5295 0.8702046,-1.109025 C 6.3771239,15.3687 5.8517967,15.2649 5.6564568,15.116025 M 5.9069151,14.562 c 0.1336019,0.18075 0.091094,0.2862 0.1459211,0.337425 0.019781,0.01875 0.048051,0.02475 0.076259,0.0135 0.075732,-0.03 0.1129727,-0.14505 0.1200954,-0.22545 v 0 c 7.9e-6,0 7.86e-5,-5.25e-4 7.86e-5,-5.25e-4 v 0 C 6.2682872,14.493825 6.1614462,14.2767 6.0197781,14.122875 v 0 l -3.774e-4,-4.5e-4 h -7.8e-6 C 5.8351063,13.9179 5.5447655,13.76025 5.2159181,13.7115 4.9162927,13.66725 4.6215415,13.72425 4.539253,13.7475 l 4.01e-4,-1.5e-4 c -0.042421,0.012 -0.094899,0.0225 -0.1399699,0.039 -0.8058172,0.3003 -1.1548142,1.05135 -0.9874148,1.79865 l -7.86e-5,-1.5e-4 c 0.041384,0.1806 0.1259288,0.38385 0.2578718,0.517425 v 0 l 4.167e-4,3.75e-4 h 7.9e-6 c 0.1650093,0.170475 0.3458522,0.137775 0.2686424,-0.021 C 3.9207015,16.0374 3.8210388,15.8583 3.8076345,15.5334 3.7939555,15.1992 3.8751585,14.85195 4.0988165,14.5866 4.2649109,14.3925 4.468993,14.3055 4.4928611,14.292825 v 0 c 0.028601,-0.01425 0.058396,-0.02475 0.090645,-0.039 0.012587,-0.0075 0.027217,-0.009 0.043223,-0.0135 0.1025718,-0.02775 0.044364,-0.01575 0.1483112,-0.03675 l -9.43e-5,7.5e-5 C 5.3291178,14.091375 5.7295853,14.3214 5.9068983,14.5614 M 17.569498,10.812225 c 0.289802,-0.024 0.571486,0.02475 0.811222,0.139425 C 18.36211,10.29255 17.960562,9.5502 17.657115,9.650775 V 9.6507 c -2.28e-4,7.5e-5 -1.1e-4,7.5e-5 -3.38e-4,7.5e-5 v 0 c -0.17749,0.0555 -0.207372,0.3531 -0.203539,0.53325 0.0086,0.2163 0.04341,0.4161 0.11626,0.6282 m -3.037628,6.7974 c 0.01126,0.08467 0.02886,0.123975 0.031,0.12855 -0.01277,-0.02625 -0.02368,-0.072 -0.031,-0.12855 m 2.503058,-5.10855 c 0.198879,0.1041 0.424457,0.0762 0.504008,-0.06225 0.07944,-0.1386 -0.0174,-0.335325 -0.216282,-0.439425 -0.198879,-0.104175 -0.424456,-0.0762 -0.503899,0.06225 -0.07944,0.13845 0.0173,0.33525 0.216173,0.439425 m 1.054621,-0.688125 c -0.03089,0.189525 0.0613,0.36045 0.205883,0.381975 0.144575,0.02175 0.286748,-0.114525 0.317745,-0.303975 0.03089,-0.189375 -0.06131,-0.36045 -0.205883,-0.381975 -0.144575,-0.02175 -0.286863,0.114525 -0.317745,0.303975 M 5.6564568,15.115425 c -0.053986,-0.04125 -0.079105,-0.07575 -0.093955,-0.129525 -0.022076,-0.0978 -0.010574,-0.15465 0.076911,-0.2133 0.067367,-0.04425 0.1216206,-0.06525 0.1218014,-0.09315 0.00305,-0.0525 -0.2223606,-0.10635 -0.3799485,0.042 -0.1308739,0.1332 -0.1721793,0.41085 0.036581,0.6273 0.2323999,0.2397 0.5938341,0.296175 0.6501554,0.59745 0.0074,0.04275 0.012909,0.0903 0.00904,0.137175 8.25e-4,0.054 -0.018035,0.1314 -0.018971,0.136275 -0.06989,0.291075 -0.3589656,0.568725 -0.8355973,0.497925 -0.087807,-0.012 -0.1447105,-0.0225 -0.1616367,6.75e-4 C 5.0240831,16.767 5.2283386,16.9905 5.6022808,16.9824 6.1362716,16.9719 6.5779109,16.4529 6.4724854,15.873375 6.3771239,15.36825 5.8517967,15.26445 5.6564568,15.115575 M 5.9069151,14.56155 c 0.1336019,0.18075 0.091094,0.2862 0.1459211,0.337425 0.019781,0.01875 0.048051,0.02475 0.076259,0.0135 0.075732,-0.03 0.1129727,-0.14505 0.1200954,-0.22545 v 0 c 7.9e-6,0 7.86e-5,-5.25e-4 7.86e-5,-5.25e-4 v 0 c 0.019018,-0.193125 -0.087823,-0.41025 -0.2294911,-0.564075 v 0 l -3.774e-4,-4.5e-4 h -7.8e-6 C 5.8351063,13.91745 5.5447655,13.7598 5.2159181,13.71105 4.9162927,13.6668 4.6215415,13.7238 4.539253,13.74705 l 4.01e-4,-1.5e-4 c -0.042421,0.012 -0.094899,0.0225 -0.1399699,0.039 -0.8058172,0.3003 -1.1548142,1.05135 -0.9874148,1.79865 l -7.86e-5,-1.5e-4 c 0.041384,0.1806 0.1259288,0.38385 0.2578718,0.517425 v 0 l 4.167e-4,3.75e-4 h 7.9e-6 c 0.1650093,0.170475 0.3458522,0.137775 0.2686424,-0.021 -0.018428,-0.04425 -0.1180907,-0.22335 -0.131495,-0.54825 -0.013679,-0.3342 0.067524,-0.68145 0.291182,-0.9468 0.1660944,-0.1941 0.3701765,-0.2811 0.3940446,-0.293775 v 0 c 0.028601,-0.01425 0.058396,-0.02475 0.090645,-0.039 0.012587,-0.0075 0.027217,-0.009 0.043223,-0.0135 0.1025718,-0.02775 0.044364,-0.01575 0.1483112,-0.03675 l -9.43e-5,7.5e-5 c 0.5541718,-0.112275 0.9546393,0.11775 1.1319523,0.35775 M 14.918385,12.4062 c 0.141223,0.009 0.235839,0.01725 0.257789,-0.0195 0.04857,-0.07883 -0.325786,-0.349725 -0.837348,-0.266775 -0.0638,0.009 -0.122823,0.02775 -0.180825,0.04275 -0.0212,0.0075 -0.04166,0.012 -0.06146,0.02025 -0.125812,0.05025 -0.234719,0.105225 -0.340356,0.201975 -0.121236,0.111975 -0.155701,0.215925 -0.120582,0.2415 0.03437,0.02625 0.118247,-0.01275 0.247982,-0.063 0.434505,-0.1725 0.741891,-0.17715 1.034799,-0.157725 M 14.832994,11.7498 c 0.226728,0.08437 0.380349,0.1491 0.423369,0.1011 0.02155,-0.02325 0.0072,-0.07972 -0.04612,-0.1542 -0.140863,-0.19695 -0.410238,-0.365325 -0.650182,-0.44955 -0.54784,-0.19485 -1.189034,-0.08437 -1.6558,0.271425 -0.23207,0.18015 -0.33489,0.36585 -0.228696,0.38085 0.06514,0.009 0.200859,-0.04425 0.392253,-0.114225 0.757041,-0.273975 1.184722,-0.242325 1.76518,-0.03525 M 12.485428,2.193675 11.926385,1.74585 12.12946,2.4201 C 12.241165,2.34735 12.360197,2.271075 12.485428,2.193675 M 11.906342,2.5686 10.753038,2.232075 l 0.762108,0.60975 c 0.11101,-0.0801 0.242456,-0.17265 0.391196,-0.273225 m -0.935538,-0.726075 0.338061,0.1047 -0.27351,-0.949275 -0.160763,0.47295 0.09621,0.371625 m 0.03986,4.61325 c 2.735857,-1.694325 5.468727,-1.8891 6.404813,-1.20135 1.26e-4,0 2.6e-4,-1.5e-4 2.6e-4,-2.25e-4 -0.0016,-0.0075 -0.01679,-0.04425 -0.02257,-0.05475 v 0 C 17.155336,4.71675 16.600685,4.38015 16.10655,4.245 16.19322,4.331025 16.318615,4.488525 16.369388,4.582125 15.996415,4.3374 15.50087,4.12185 14.983785,4.019475 c 0,0 0.06155,0.042 0.07219,0.05175 0.102955,0.0918 0.240144,0.2367 0.296301,0.3612 -0.494263,-0.189675 -1.119047,-0.295875 -1.653954,-0.19755 -0.0074,0 -0.0668,0.015 -0.0668,0.015 0,0 0.06962,0.0165 0.08513,0.021 0.17732,0.05025 0.430413,0.153525 0.541959,0.295125 -0.887879,-0.149325 -1.864519,0.01575 -2.391322,0.297375 0.06776,0 0.06713,0 0.124367,0 0.193718,0.0075 0.584128,0.03 0.748036,0.129675 -0.560063,0.108975 -1.370526,0.3483 -1.806848,0.7068 0.07704,-0.009 0.514225,-0.0615 0.693083,-0.0345 -2.4015149,1.313175 -3.4945354,3.2985 -3.4945354,3.2985 V 8.9637 C 8.8394242,8.08215 9.7580608,7.2336 11.010668,6.4578 m 11.883969,10.3458 c -0.03284,0.0615 -0.376935,1.842825 -2.347098,3.3219 -2.487373,1.86765 -5.755581,1.67865 -6.989917,0.632025 -0.65935,-0.589275 -0.944734,-1.43205 -0.944734,-1.43205 0,0 -0.07469,0.4749 -0.08751,0.6615 -0.497332,-0.80835 -0.455264,-1.795575 -0.455264,-1.795575 0,0 -0.265347,0.472725 -0.386875,0.737625 -0.36605,-0.890325 -0.176893,-1.8096 -0.176893,-1.8096 l -0.28963,0.4125 c 0,0 -0.135771,-1.00755 0.197648,-1.846875 0.356296,-0.896775 1.047258,-1.5483 1.183818,-1.629525 -0.52422,-0.15915 -1.128163,-0.6156 -1.129182,-0.616425 0,0 0.240104,0.015 0.407085,-0.02175 0,0 -1.059837,-0.72555 -1.245694,-1.83555 0.153542,0.18135 0.475716,0.386025 0.475716,0.386025 -0.104318,-0.290475 -0.167369,-0.93675 -0.06989,-1.57275 l 3.14e-4,-6.75e-4 c 0.200473,-1.215075 1.248607,-2.00625 2.435724,-1.996425 1.263911,0.0105 2.110724,0.264075 3.170246,-0.66945 0.224182,-0.197475 0.403183,-0.368025 0.71787,-0.434625 0.03328,-0.0075 0.115606,-0.03975 0.284423,-0.03975 0.170969,0 0.335627,0.03675 0.486229,0.122325 0.574584,0.326925 0.698508,1.179375 0.760326,1.804275 0.228342,2.318325 0.135972,1.905525 1.117898,2.383425 0.468444,0.2274 0.994835,0.443925 1.593801,1.0566 0.0017,0 0.0044,0.0075 0.0044,0.0075 0,0 0.0053,0 0.0073,0 0.505019,0.01125 0.765346,0.391575 0.532415,0.66825 -1.695063,1.9341 -4.062385,2.860275 -6.700647,2.93775 -0.109007,0 -0.35413,0.0083 -0.35542,0.0083 -1.065518,0.03075 -1.412462,1.347975 -0.743832,2.140125 0.422547,0.5007 1.235368,0.665175 1.904328,0.667575 h 0.0093 c 2.884675,0.0555 5.78269,-1.8945 6.283263,-2.970075 0.0036,-0.0075 0.03428,-0.0762 0.03428,-0.0762 -0.116036,0.1299 -2.925697,2.657625 -6.340205,2.566125 0,0 -0.37335,-0.0075 -0.724898,-0.08565 -0.463424,-0.102975 -0.815546,-0.2976 -0.950372,-0.738975 0.283275,0.054 0.641852,0.08895 1.058088,0.08895 2.464424,0 4.240526,-1.07055 4.054926,-1.084875 -0.0072,7.5e-5 -0.01478,0 -0.02682,0.0075 -0.287291,0.06375 -3.24985,1.16025 -5.122623,0.598125 0.0046,-0.05475 0.01319,-0.107775 0.02668,-0.155325 0.166523,-0.53295 0.462565,-0.458475 0.941048,-0.47835 v 0 c 1.708689,-0.054 3.08763,-0.464925 4.120761,-0.933675 1.101978,-0.499875 1.941764,-1.1439 2.244401,-1.469025 0.392427,0.63135 0.389988,1.442025 0.389988,1.442025 0,0 0.154045,-0.05175 0.357716,-0.05175 0.640418,7.5e-5 0.77223,0.547425 0.287435,1.103025 m -17.8914758,1.18875 c -1.4035665,0 -2.5413837,-1.1445 -2.5413837,-2.55645 0,-1.411875 1.1378172,-2.556375 2.5413837,-2.556375 0.3638477,0 0.7098415,0.07687 1.0227768,0.2154 0,0 0.5401859,0.260325 0.6921291,1.4901 v 0 0 c 0.158209,-0.384225 0.2379504,-0.6996 0.2379504,-0.6996 0.1810865,0.528975 0.2737682,1.085475 0.2373921,1.647975 v 0 0 c 0.1501508,-0.19095 0.3113551,-0.5505 0.3113551,-0.5505 0.2792947,1.571175 -0.9206689,3.00945 -2.5016114,3.00945 m -3.8747945,-5.3622 C 0.33205342,11.354775 1.7172778,8.7177 2.7030001,7.2189 5.1390722,3.514425 9.1937474,0.590925 11.035369,1.008375 l 0.50653,-0.1854 c 0.0017,0 1.382945,1.116825 1.384688,1.11825 0.951185,-0.545775 2.163307,-1.102125 3.29636,-1.21275 -0.689541,0.14835 -1.529915,0.49005 -2.525451,1.07205 -0.02428,0.0135 -2.355922,1.517025 -3.7803918,2.86725 -0.7763353,0.735675 -3.8935922,4.3074 -3.8911276,4.30455 0.569698,-1.03035 0.9450529,-1.536 1.8469266,-2.619825 0.5100621,-0.6129 1.054331,-1.209 1.6115696,-1.759425 0.2586532,-0.2556 0.5201462,-0.5013 0.7823312,-0.73425 0.180186,-0.160275 0.360777,-0.314475 0.540887,-0.4617 0.08293,-0.0675 0.165802,-0.1341 0.248508,-0.1989 1.5e-4,0 3.15e-4,-1.5e-4 3.93e-4,-3e-4 L 9.2295186,1.756425 9.3262166,2.40255 10.65423,3.5205 c 0,0 -1.1752564,0.755925 -1.7597725,1.23255 C 6.5514886,6.6633 4.2523926,9.595875 3.3968241,12.450525 h 0.040789 C 3.0115192,12.67485 2.5885821,13.03485 2.2191935,13.5246 c -0.010009,0 -0.9563368,-0.66615 -1.0909041,-0.884625 M 23.915514,15.62775 C 23.742466,15.274725 23.412705,15.0312 22.998808,14.937825 22.860739,14.33415 22.669046,14.0376 22.651634,13.99365 c 0.07304,-0.07943 0.143615,-0.1593 0.159332,-0.1779 0.584326,-0.6924 0.203097,-1.707075 -0.795742,-1.9461 C 21.453397,11.35305 20.94504,11.110275 20.527445,10.910925 20.127109,10.71975 20.287058,10.794675 19.91153,10.63275 19.81183,10.166325 19.778855,9.080925 19.620139,8.31885 19.477601,7.633425 19.190524,7.137225 18.747811,6.810825 c -0.177054,-0.366675 -0.425454,-0.7359 -0.724859,-1.0077 1.392549,-2.03985 1.759139,-4.0548 0.73919,-5.1099 C 18.307872,0.22305 17.633402,0 16.826718,0 15.69089,1.5e-4 14.293687,0.442725 12.882832,1.26135 c 0,0 -0.91817,-0.706125 -0.937988,-0.721125 C 8.0154886,-2.4162 -3.0965673,10.64925 0.82607453,13.5045 l 1.01259507,0.73935 c -0.6356513,1.68975 0.2483538,3.703875 2.0908269,4.350675 0.4073017,0.14295 0.8489206,0.2124 1.3066112,0.188025 0,0 2.9765604,5.21565 9.2564843,5.21745 7.264679,0 9.113655,-6.7887 9.133225,-6.849825 0,0 0.588794,-0.831375 0.289697,-1.522425 m -19.9672492,0.5661 c 0,7.5e-5 0,7.5e-5 -7.87e-5,1.5e-4 0,0 -7.86e-5,7.5e-5 -8.65e-5,7.5e-5 l 1.415e-4,-2.25e-4 M 6.0752106,14.9241 c 0,0 -7.86e-5,0 -8.65e-5,-7.5e-5 0,0 -7.86e-5,0 -8.64e-5,0 l 1.729e-4,7.5e-5 M 4.1312854,13.920675 c 0,0 7.86e-5,0 9.42e-5,-7.5e-5 0,0 7.87e-5,0 1.023e-4,0 l -1.965e-4,7.5e-5 m 1.0845776,-0.19875 v 0 c 7.8e-6,0 0,0 0,0 v 0" />');
}]);
'use strict';

angular.module('am-wb-mailchimp')
/**
 * @ngdoc Controllers
 * @name AmWbMailchimpSubscribeCtrl
 * @description Mail chimp controller
 * 
 * Manage connection and post into the list
 */
.controller('AmWbMailchimpSubscribeCtrl', function($log, $http, $scope, $rootScope) {

	var wbModel = $scope.wbModel;

	// Create a resource for interacting with the MailChimp API
	function addSubscription(mailchimp) {
		var params = {};
		var url = '//' + wbModel.username + '.' + wbModel.dc+ '.list-manage.com/subscribe/post-json';

		var fields = Object.keys(mailchimp);
		for (var i = 0; i < fields.length; i++) {
			params[fields[i]] = mailchimp[fields[i]];
		}
		params.u = wbModel.u;
		params.id = wbModel.id;

		// Send subscriber data to MailChimp
		$http({
			url : url,
			method : 'JSONP',
			params : params,
			jsonpCallbackParam : 'c'
		})
		// Successfully sent data to MailChimp.
		.then(function(res) {
			var response = res.data;
			// Define message containers.
			mailchimp.errorMessage = '';
			mailchimp.successMessage = '';

			// Store the result from MailChimp
			mailchimp.result = response.result;

			// Mailchimp returned an error.
			if (response.result === 'error') {
				if (response.msg) {
					// Remove error numbers, if
					// any.
					var errorMessageParts = response.msg
					.split(' - ');
					if (errorMessageParts.length > 1){
						// Remove the error number
						errorMessageParts.shift(); 
					}
					mailchimp.errorMessage = errorMessageParts.join(' ');
				} else {
					mailchimp.errorMessage = 'Sorry! An unknown error occured.';
				}
			}
			// MailChimp returns a success.
			else if (response.result === 'success') {
				mailchimp.successMessage = response.msg;
			}

			// Broadcast the result for global
			// msgs
			$rootScope.$broadcast(
					'mailchimp-response',
					response.result,
					response.msg);
		},// Error sending data to MailChimp
		function(error) {
			$log.error('MailChimp Error: %o', error);
		});
	}

	// Handle clicks on the form submission.
	$scope.addSubscription = addSubscription;
});
/* 
 * The MIT License (MIT)
 * 
 * Copyright (c) 2016 weburger
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';

angular.module('am-wb-mailchimp')
/*
 * 
 * defines and registers widgets in this module. Following widgets exist in this
 * module:
 *  - MainchimpSubscribe
 * 
 */
.run(function($widget, $settings) {
	/**
	 * @ngdoc Widgets
	 * @name MailchimpSubscribe
	 * @description Submit into the list of mailchimp
	 * 
	 */
	$widget.newWidget({
		// widget
		type : 'MailchimpSubscribe',
		title : 'Mailchimp subscribe',
		description : 'Mailchimp subscribe widget.',
		groups : [ 'form', 'marketing', 'mail' ],
		icon : 'wb-mailchimp',
		// help
		help : 'https://gitlab.com/weburger/am-wb-mailchimp/wikis/home',
		// page
		templateUrl : 'views/am-wb-mailchimp-widgets/subscribe.html',
		controller : 'AmWbMailchimpSubscribeCtrl',
		setting : [ 'mailchimp-subscribe' ]
	});

	/**
	 * @ngdoc Widget settings
	 * @name mailchimp-subscribe
	 * @description manage settings of a mailchimp widget
	 * 
	 */
	$settings.newPage({
		type : 'mailchimp-subscribe',
		label : 'Mailchimp',
		description : 'Manage Milchimp attribute',
		icon : 'wb-mailchimp',
		templateUrl : 'views/am-wb-mailchimp-settings/subscribe.html'
	});
});

angular.module('am-wb-mailchimp').run(['$templateCache', function($templateCache) {
  'use strict';

  $templateCache.put('views/am-wb-mailchimp-settings/subscribe.html',
    "<md-list class=wb-setting-panel>  <md-input-container class=\"md-icon-float md-block\"> <label translate>User name</label> <input ng-model=wbModel.username> </md-input-container> <md-input-container class=\"md-icon-float md-block\"> <label translate>DC</label> <input ng-model=wbModel.dc> </md-input-container> <md-input-container class=\"md-icon-float md-block\"> <label translate>U</label> <input ng-model=wbModel.u> </md-input-container> <md-input-container class=\"md-icon-float md-block\"> <label translate>ID</label> <input ng-model=wbModel.id> </md-input-container> </md-list>"
  );


  $templateCache.put('views/am-wb-mailchimp-widgets/subscribe.html',
    " <div ng-show=\"mailchimp.result === 'error'\" ng-cloak> <span ng-bind-html=mailchimp.errorMessage></span> </div>  <div ng-show=\"mailchimp.result === 'success'\" ng-cloak> <span ng-bind-html=mailchimp.successMessage></span> </div> <form name=MailchimpSubscriptionForm ng-hide=\"mailchimp.result === 'success'\" ng-submit=addSubscription(mailchimp) layout=column layout-padding> <md-input-container class=\"md-icon-float md-block\"> <label translate>First name</label> <input name=fname ng-model=mailchimp.FNAME> </md-input-container> <md-input-container class=\"md-icon-float md-block\"> <label translate>Last name</label> <input name=lname ng-model=mailchimp.LNAME> </md-input-container> <md-input-container class=\"md-icon-float md-block\"> <label translate>Email</label> <input type=email name=email ng-model=mailchimp.EMAIL required> </md-input-container> <div layout=row> <span flex></span> <md-button ng-disabled=MailchimpSubscriptionForm.$invalid ng-click=addSubscription(mailchimp) aria-label=\"Add featrue\"> <span translate>Submit</span> </md-button> </div> <input ng-hide=true type=submit ng-disabled=MailchimpSubscriptionForm.$invalid ng-click=\"addSubscription(mailchimp)\"> </form>"
  );

}]);
