/* 
 * The MIT License (MIT)
 * 
 * Copyright (c) 2016 weburger
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';

angular.module('am-wb-mailchimp')
/*
 * 
 * defines and registers widgets in this module. Following widgets exist in this
 * module:
 *  - MainchimpSubscribe
 * 
 */
.run(function($widget, $settings) {
	/**
	 * @ngdoc Widgets
	 * @name MailchimpSubscribe
	 * @description Submit into the list of mailchimp
	 * 
	 */
	$widget.newWidget({
		// widget
		type : 'MailchimpSubscribe',
		title : 'Mailchimp subscribe',
		description : 'Mailchimp subscribe widget.',
		groups : [ 'form', 'marketing', 'mail' ],
		icon : 'wb-mailchimp',
		// help
		help : 'https://gitlab.com/weburger/am-wb-mailchimp/wikis/home',
		// page
		templateUrl : 'views/am-wb-mailchimp-widgets/subscribe.html',
		controller : 'AmWbMailchimpSubscribeCtrl',
		setting : [ 'mailchimp-subscribe' ]
	});

	/**
	 * @ngdoc Widget settings
	 * @name mailchimp-subscribe
	 * @description manage settings of a mailchimp widget
	 * 
	 */
	$settings.newPage({
		type : 'mailchimp-subscribe',
		label : 'Mailchimp',
		description : 'Manage Milchimp attribute',
		icon : 'wb-mailchimp',
		templateUrl : 'views/am-wb-mailchimp-settings/subscribe.html'
	});
});
